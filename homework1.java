public class homework1 {
    public static void main(String[] args) {
        sortArray();
        sumInputNumbers(new int[]{3, 5, 0, -11, 12, 15});
        sumInputNumbers(new int[]{});
        averageNumber(new int[]{1, 1, 3, 4, 5, 6});
        replaceDuplicates(new int[]{3, 2, 3, 1, 4, 2, 8, 3});
    }

    public static void sortArray() {
        int[] array = {2, 3, 1, 7, 11};

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] < array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (i + 1 == array.length) {
                System.out.println(array[i]);
            } else {
                System.out.print(array[i] + ", ");
            }
        }
    }

    public static int sumInputNumbers(int[] input) {
        int sum = 0;
        for (int el : input) {
            if (el > 0) {
                sum += el;
            }
        }
        System.out.println(sum);
        return sum;
    }

    public static void averageNumber(int[] input) {
        System.out.println(sumInputNumbers(input) / input.length);
    }

    public static void replaceDuplicates(int[] input) {
        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length; j++) {
                if (input[i] == input[j]) {
                    input[j] = 0;
                }
            }
        }
        for (int i = 0; i < input.length; i++) {
            if (i + 1 == input.length) {
                System.out.println(input[i]);
            } else {
                System.out.print(input[i] + ", ");
            }
        }
    }
}
